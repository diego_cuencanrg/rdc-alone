from service.RDCExportService import RDCExportService
from service.RDCPrimaryService import RDCPrimaryService
import sys

def main():
	date = sys.argv[1]
	cleaner =RDCPrimaryService()
	curveData = cleaner.setUp(date)
	exporter = RDCExportService()
	exporter.run(curveData=curveData,Tdatabase=False)

main()

