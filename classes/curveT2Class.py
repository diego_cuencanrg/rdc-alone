from classes.curveClass import CurveClass
from utils.sorters import sortNegative


class CurveT2Class (CurveClass):
    def __init__(self,path,dataFromDict=False):
        if not dataFromDict:
            self.name = path.split("\\")[-1]
            self.loadFile(path)
            self.offset = 0
        else:
            self.listPoints = path
            self.name = "Curve2"
            self.offset = 0

    def loadFile(self, path):
        CurveClass.loadFile(self, path)
        self.listPoints.sort(key=sortNegative())


