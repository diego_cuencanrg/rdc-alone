from constants import INTERVAL_CUT


class CurveClass():
    def __init__(self, path, listPoints):
        self.name = path.split("\\")[-1]
        self.listPoints = listPoints
        self.offset = 0

    def __init__(self,path):
        self.loadFile(path)
        self.offset = 0

    def getName(self):
        return self.name

    def getListPoints(self):
        return self.listPoints

    def applyOffSet(self):
        self.listPoints[0][1] += INTERVAL_CUT
        self.offset += INTERVAL_CUT

    def applyDefaultValueCurve(self):
        self.listPoints[0][1] -=  self.offset
        self.offset += 0

    def loadFile(self,path):
        self.listPoints = []
        with open(path) as file:
            for line in file:
                priceValor = float(line.split(";")[1])
                energyValor = float(line.split(";")[0])
                self.listPoints += [[priceValor, energyValor]]

    def printPoints(self):
        for point in self.listPoints:
            print("Price: " + str(point[0]) + " - Energy: " + str(point[1]))

    def __str__(self):
        return "Name: " + self.name + "\n" + "Points: " + str(len(self.listPoints)) + "\n"

