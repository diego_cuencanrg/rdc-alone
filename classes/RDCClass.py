from constants import OUTPUT_FILE, EXTENSION_FILE, MAX_INTERVAL_VALUE, INTERVAL_CUT
from utils.tools import createDirectory


class RDCClass():

    def __init__(self,fileName,curve1,curve2,date=None,dataFromDict=False):
        if dataFromDict:

            self.__date = date.split("_")[0][:4] + "-" + date.split("_")[0][4:6] + "-" + date.split("_")[0][6:]
            self.__hour = date.split("_")[1]
        else:

            self.__date = fileName.split("_")[1]
            self.__hour = fileName.split("_")[2]
            self.__type = fileName.split("_")[4][0]
        self.__curve1 = curve1
        self.__curve2 = curve2
        self.__cutPoint = self.calculateCutPoint()


    def calculateCutPoint(self):
        cutPoint_price = (self.__match(self.__curve1.getListPoints(), self.__curve2.getListPoints()))

        return cutPoint_price # Returns the P (P,V)

    def exportToModelFile (self, interval, toBD=False, capacity = False):

        countInterval = interval
        OperationResult = {}
        OperationResult [0] = self.__cutPoint
        while countInterval<=MAX_INTERVAL_VALUE: #Curve1
            self.__curve1.applyOffSet()
            resultQP = self.calculateCutPoint()
            OperationResult[countInterval]=resultQP
            countInterval+=interval
        self.__curve1.applyDefaultValueCurve()

        if capacity == False:
            countInterval = interval
            while countInterval<=MAX_INTERVAL_VALUE: #Curve2
                self.__curve2.applyOffSet()
                resultQP = self.calculateCutPoint()
                OperationResult[-countInterval] = resultQP
                countInterval += interval
            self.__curve2.applyDefaultValueCurve()


        createDirectory(OUTPUT_FILE)
        with open(OUTPUT_FILE + "/" + self.generateNameFile(), "w") as file:
            for key in sorted(OperationResult, reverse=False):
                file.write(str(key) +";" + str(OperationResult[key]) + "\n")


    def __match(self, a, b):
        cont_a = 0
        cont_b = 0

        mw_a = a[0][1]
        mw_b = b[0][1]
        price_a = 0
        price_b = 0

        output = 0
        look_a = True
        out = True

        eps = 0.0001
        while out:
            price_a = a[cont_a][0]
            price_b = b[cont_b][0]
            if look_a:
                if b[cont_b][0] <= a[cont_a][0] + eps:
                    output = a[cont_a][0]
                    out = False
                else:
                    if mw_b >= mw_a - eps:
                        if cont_a + 1 >= len(a):
                            output = b[cont_b][0]
                            out = False
                        else:
                            look_a = False
                            cont_a += 1
                            mw_a += a[cont_a][1]
                    else:
                        if cont_b + 1 >= len(b):
                            output = b[cont_b][0]
                            out = False
                        else:
                            cont_b += 1
                            mw_b += b[cont_b][1]
            else:
                if b[cont_b][0] <= a[cont_a][0] + eps:
                    output = b[cont_b][0]
                    out = False
                else:
                    if mw_a >= mw_b - eps:
                        if cont_b + 1 >= len(b):
                            output = b[cont_b][0]
                            out = False
                        else:
                            look_a = True
                            cont_b += 1
                            mw_b += b[cont_b][1]
                    else:
                        if cont_a + 1 >= len(a):
                            output = a[cont_a][0]
                            out = False
                        else:
                            cont_a += 1
                            mw_a += a[cont_a][1]
        return round(output, 4)


    #Basics methods
    def getCurve1(self):
        return self.__curve1

    def getCurve2(self):
        return self.__curve2

    def getCutPoint(self):
        return self.__cutPoint

    def generateNameFile(self):
        return "RDC_" +self.__date + "_" + self.__hour + EXTENSION_FILE

    def __str__(self):
        return "Name: " + self.generateNameFile() + "\n" + "Curve T1: " + str(self.__curve1) + "Curve T2: " + str(self.__curve2) + "Cut Point: " + str(self.__cutPoint) + "\n"

