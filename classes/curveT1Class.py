from operator import itemgetter

from classes.curveClass import CurveClass
from utils.sorters import sortPositive


class CurveT1Class (CurveClass):
    def __init__(self,path,dataFromDict=False):
        if not dataFromDict:
            self.name =path.split("\\")[-1]
            self.offset = 0
            self.loadFile(path)
        else:
            self.name = "Curve1"
            self.offset = 0
            self.listPoints = path

    def loadFile(self, path):
        CurveClass.loadFile(self, path)
        self.listPoints.sort(key=sortPositive)

