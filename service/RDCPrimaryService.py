from pandas import DataFrame
import service.inputs_parser as parser

import utils.RDCTools as aux

import logging



class RDCPrimaryService:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def setUp(self,date):
        headers_curves = ['id','TimeSeries_FID', 'PublicationDate', 'QuoteDateIndex_FID', 'QuoteTime', 'DeliveryDate','DeliveryStartDate',
                   'DeliveryEndDate', 'price', 'volume','sense']
        headers_MinMax = ['id','Delivery Date', 'Context', 'P_Type', 'Application', 'L_Type', 'Fetch Time',
                          'EMIS Sum', 'H01', 'H02', 'H03', 'H04', 'H05', 'H06', 'H07', 'H08', 'H09', 'H10',
                          'H11', 'H12', 'H13', 'H14', 'H15', 'H16', 'H17', 'H18', 'H19', 'H20', 'H21', 'H22', 'H23',
                          'H24', 'H25', 'Total', 'Powerstation Element']
        headers_marginalPrices = ['id', 'Delivery Date', 'Context', 'Powerstation Element', 'Type', 'Price From', 'Price To']

        date_original = date[0:4] + "-" + date[4:6] + "-" + date[6:8]

        # Units list to adjust
        UNITS_FILE = "units.txt"
        # Load an arbitrary set of data to separate pumps and turbines
        dt_1 = parser.get_volumes(date)
        dt_1.fillna(0)


        dt_2 = parser.get_prices(date)


        dt_1 = dt_1[dt_1['Application'] == "EXPRO"]
        dt_1 = dt_1[dt_1['L_Type'] == 'Max']
        self.logger.info("Load entities to delete")
        list_turbines = aux.list_enti('Turbine', UNITS_FILE, dt_1)
        list_pumps = aux.list_enti('Pump', UNITS_FILE, dt_1)


        askfile_info = aux.string_cleaner2('ask', date_original.split("-")[1], date_original.split("-")[0])
        bidfile_info = aux.string_cleaner2('bid', date_original.split("-")[1], date_original.split("-")[0])

        dt_ask = parser.get_ask(date)

        dt_bid = parser.get_bid(date)

        dt_ask = dt_ask.sort_values(['volume','price'], ascending=(True,True))
        dt_bid = dt_bid.sort_values(['volume','price'], ascending=(True,False))


        curveData = {}

        # Load diary volume and price files
        month = date_original.split("-")[1]
        day = date_original.split("-")[2]

        hour = date.split("_")[1]
        DAY_VOLUME_FILE = parser.get_volumes(date)


        DAY_PRICE_FILE = parser.get_prices(date)

        # Look for Axpo participation for that day
        if DAY_VOLUME_FILE is not None and DAY_PRICE_FILE is not None:
            # Get the dataframe summary
            aux_dt_turbines = aux.auxdict(DAY_VOLUME_FILE, DAY_PRICE_FILE, UNITS_FILE)["turbine"]
            aux_dt_pumps = aux.auxdict(DAY_VOLUME_FILE, DAY_PRICE_FILE, UNITS_FILE)["pump"]

            # for z in range(1, 24 + 1):
            print("Date: " + str(date_original) + " Hora: "  + str(hour))
            dt_ask_res = aux.filter_h(dt_ask, int(hour) - 1, askfile_info['year'], month, day)
            dt_bid_res = aux.filter_h(dt_bid, int(hour) - 1, bidfile_info['year'], month, day)

            # Add column of acumulated volume
            dt_ask_res = aux.acumulator(dt_ask_res, type="ask")
            dt_bid_res = aux.acumulator(dt_bid_res, type="bid")

            ## Look market activity for that hour
            if dt_ask_res.size == 0 or dt_bid_res.size == 0:
                pass

                # TURBINES == ASK (CURVE 1)
            for turbine in list_turbines:
                aux_turbine = aux_dt_turbines[aux_dt_turbines['Powerstation Element'] == turbine]
                row_turbine = aux_turbine[aux_turbine['Hour'] == int(hour)]
                price_turbine_from = int(row_turbine['Price From'])
                price_turbine_to = int(row_turbine['Price To'])
                dt_ask_res2 = aux.filter_p_ask(dt_ask_res, price_turbine_from, price_turbine_to)

                dict_volume_ask = aux.searchVolumeBlock(dt_ask_res2, aux.prices_range(row_turbine['Price From'],row_turbine['Price To']),aux.vol_range(row_turbine), sense="ask")

                dt_ask_res2 = aux.sub_volumes(dt_ask_res2, aux.prices_range(row_turbine['Price From'],row_turbine['Price To']),dict_volume_ask, sense="ask")
                # Introduce the modifications in the daily dataframe
                dt_ask_res.update(dt_ask_res2)


            # PUMPS = BID (CURVA 2)
            for pump in list_pumps:
                aux_pump = aux_dt_pumps[aux_dt_pumps['Powerstation Element'] == pump]
                row_pump = aux_pump[aux_pump['Hour'] == int(hour)]
                price_pump_from = int(row_pump['Price From'])
                price_pump_to = int(row_pump['Price To'])
                dt_bid_res2 = aux.filter_p_bid(dt_bid_res, price_pump_from, price_pump_to)

                dict_volume_bid = aux.searchVolumeBlock(dt_bid_res2, aux.prices_range(row_pump['Price From'], row_pump['Price To']),aux.vol_range(row_pump), sense="bid")

                dt_bid_res2 = aux.sub_volumes(dt_bid_res2,aux.prices_range(row_pump['Price From'], row_pump['Price To']),dict_volume_bid, sense="bid")
                # Introduce the modifications in the daily dataframe
                dt_bid_res.update(dt_bid_res2)

            # Delete the columns that we don't need
            dt_bid_result = dt_bid_res.drop(
                columns=['TimeSeries_FID', 'PublicationDate', 'QuoteDateIndex_FID', 'QuoteTime',
                         'DeliveryEndDate', 'volume'], axis=1)
            dt_ask_result = dt_ask_res.drop(
                columns=['TimeSeries_FID', 'PublicationDate', 'QuoteDateIndex_FID', 'QuoteTime',
                         'DeliveryEndDate', 'volume'], axis=1)

            # Order the rows
            dt_ask_result = dt_ask_result.drop(columns=['DeliveryStartDate'], axis=1)
            dt_bid_result = dt_bid_result.drop(columns=['DeliveryStartDate','sense'], axis=1)

            dt_ask_result.to_csv(path_or_buf="files/output/ask_clean.csv", index=False)
            dt_bid_result.to_csv(path_or_buf="files/output/bid_clean.csv", index=False)

            curveData[str(askfile_info['year']) + month + day + "_" + hour] = {}
            curveData[str(askfile_info['year']) + month + day + "_" + hour]['Curva1'] = [[x[0],x[1]] for x in
                                                                                       dt_ask_result.values]
            curveData[str(askfile_info['year']) + month + day + "_" + hour]['Curva2'] = [[x[0],x[1]] for x in
                                                                                       dt_bid_result.values]
            curveData[str(askfile_info['year']) + month + day + "_" + hour]['market'] = "SW-CDR"
            curveData[str(askfile_info['year']) + month + day + "_" + hour]['type'] = "C"




        else:
            if DAY_PRICE_FILE is None:
                self.logger.warning("Data: Day Volume doesn't exists")
            elif DAY_VOLUME_FILE is None:
                self.logger.warning("Data: Day Price doesn't exists")
            else:
                self.logger.warning("Data: Day Volume doesn't exists")
                self.logger.warning("Data: Day Price doesn't exists")

        return curveData