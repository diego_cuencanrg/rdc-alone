import logging

from classes.RDCClass import RDCClass
from classes.curveT1Class import CurveT1Class
from classes.curveT2Class import CurveT2Class
from constants import INTERVAL_CUT


class RDCExportService:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def run(self,curveData, Tdatabase):
        for key,value in curveData.items():
            try:
                curve1 = CurveT1Class(value['Curva1'], dataFromDict=True)
                curve2 = CurveT2Class(value['Curva2'], dataFromDict=True)
                cdrCurve = RDCClass(value, curve1, curve2, date=key, dataFromDict=True)
                cdrCurve.exportToModelFile(INTERVAL_CUT, toBD=Tdatabase)
            except :
                self.logger.exception("Exception in RDCExportService")

