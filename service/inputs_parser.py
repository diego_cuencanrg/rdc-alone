import csv
import pandas as pd
from constants import INPUT_FILE, Months

# Deprecated method for loading bid_ask clean curves for RDC calculations
def askbid_parser(mode, date):
	if (mode == "ask"):
		filename = "ask_" + str(date) + ".csv"
	elif (mode == "bid"):
		filename = "bid_" + str(date) + ".csv"
	filename = "files/input/" + filename
	with open(filename) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=';', quoting=csv.QUOTE_NONNUMERIC)
		curve = [[x[0], x[1]] for x in csv_reader]

	return curve


def get_ask(date):
	headers_curves = ['TimeSeries_FID', 'PublicationDate', 'QuoteDateIndex_FID', 'QuoteTime','DeliveryStartDate','DeliveryEndDate', 'price', 'volume']

	filename = INPUT_FILE + "/CH DAM aggregated curves - ask - " + Months[date[4:6]] + str(date[2:4]) + ".csv"
	df = pd.read_csv(filename, names=headers_curves, skiprows=1, sep=",")


	return df

def get_bid(date):
	headers_curves = ['TimeSeries_FID', 'PublicationDate', 'QuoteDateIndex_FID', 'QuoteTime','DeliveryStartDate','DeliveryEndDate', 'price', 'volume', 'sense']

	filename = INPUT_FILE + "/CH DAM aggregated curves - bid - " + Months[date[4:6]] + str(date[2:4]) + ".csv"
	df = pd.read_csv(filename, names=headers_curves, skiprows=1, sep=",")
	return df

def get_prices(date):
	headers_marginalPrices = ['Delivery Date', 'Context', 'Powerstation Element', 'Type', 'Price From','Price To']
	filename = INPUT_FILE + "/EXPRO-Marginal-Prices_" + str(date.split("_")[0]) + ".csv"
	df = pd.read_csv(filename, names=headers_marginalPrices, skiprows=1, sep=";")
	return df

def get_volumes(date):
	headers_MinMax = ['Delivery Date', 'Context', 'Powerstation Element', 'P_Type', 'Application', 'L_Type', 'Fetch Time','EMIS Sum', 'H01', 'H02', 'H03', 'H04', 'H05', 'H06', 'H07', 'H08', 'H09', 'H10','H11', 'H12', 'H13', 'H14', 'H15', 'H16', 'H17', 'H18', 'H19', 'H20', 'H21', 'H22', 'H23','H24', 'H25', 'Total']
	filename = INPUT_FILE + "/EXPRO-MOR_Min_Max_Vergleich_" + str(date.split("_")[0]) + ".csv"
	df = pd.read_csv(filename, names = headers_MinMax, skiprows=1, sep=";")
	return df