import pandas as pd
import calendar
import os
from calendar import monthrange
from datetime import datetime

# Given and indication (ask or bid), returns a list of the curves in the directory
from pandas import DataFrame

def list_curv(type,route):
    curves_list = []
    for filename in os.listdir(route):
        file_type = filename.split("-")[1].lstrip()
        if type in file_type:
            curves_list.append(filename)
    return curves_list

# Given and indication (turbine or pump), returns a list of entities
def list_enti(type,filename,dt):
    dt_turbines = dt[dt['P_Type'] == type]
    list_entities = dt_turbines['Powerstation Element'].tolist()
    list_ent = []
    with open (filename,'r') as file:
        for line in file:
            linea = line.strip("\n")
            if( linea in list_entities):
                list_ent.append(linea)
    return list_ent

# Dado un filename devuelve su tipo (ask,dib), su mes y año
# def string_cleaner(name):
#     # print(name)
#     filename_info = {
#         "type": name.split("-")[1].lstrip(),
#         "month": list(calendar.month_abbr).index(name.split("-")[2].lstrip()[0:-6]),
#         "year": 2000 + int(name.split("-")[2][-6:-4])
#     }
#     filename_info["month_days"] = getmonth_days(filename_info["year"],filename_info["month"])
#     return filename_info

def string_cleaner2(type, month, year):
    filename_info = {
        "type": type,
        "month": calendar.month_abbr[int(month)],
        "year": year
    }
    filename_info["month_days"] = getmonth_days(filename_info["year"], month)
    return filename_info

# Given and date and hour expression, returns the electric hour
def get_elect(query):
    date_output = datetime.strptime(query,'%Y-%m-%d %H:%M:%S.%f %z')
    return(date_output.hour+1)

# Given a month and year, returns the number of days
def getmonth_days(year,month):
    return monthrange(int(year),int(month))[1]

# Given files of volumes, prices, units to delete
# Returns of a dictionary with summaries of pumps and turbines
def auxdict(Vol_file, Price_file, units_file):
    headers_MinMax = ['id', 'Delivery Date', 'Context Powerstation Element', 'P_Type', 'Application', 'L_Type',
                      'Fetch Time',
                      'EMIS Sum', 'H01', 'H02', 'H03', 'H04', 'H05', 'H06', 'H07', 'H08', 'H09', 'H10',
                      'H11', 'H12', 'H13', 'H14', 'H15', 'H16', 'H17', 'H18', 'H19', 'H20', 'H21', 'H22', 'H23',
                      'H24', 'H25', 'Total', 'Powerstation Element']
    headers_marginalPrices = ['id', 'Delivery Date', 'Context Powerstation Element', 'Entity', 'Type', 'Price From',
                              'Price To']
    # AXPO FILES
    VOLUME_FILE = Vol_file
    PRICE_FILE = Price_file

    # Units to delete
    UNITS_FILE = units_file
    unit_list = []
    f = open(UNITS_FILE, "r")
    for x in f:
        unit_list.append(x.strip("\n"))

    # Save the dataframes
    dt_volume = VOLUME_FILE

    # dt_volume.columns = headers_MinMax
    dt_volume = dt_volume.fillna(0)
    dt_volume = dt_volume.drop(['Delivery Date'],
                               axis=1)

    dt_price = PRICE_FILE

    # Masks
    Power_mask = dt_volume['Powerstation Element'].isin(unit_list)
    App_mask = dt_volume['Application'] == "EXPRO"
    Turbine_mask = dt_volume['P_Type'] == 'Turbine'
    Pump_mask = dt_volume['P_Type'] == 'Pump'
    min_mask = dt_volume['L_Type'] == "Min"
    max_mask = dt_volume['L_Type'] == "Max"

    # Volumes for turbines and pumps
    volume_fil_p_min = dt_volume[(Power_mask & App_mask & Pump_mask & min_mask)]
    volume_fil_p_max = dt_volume[(Power_mask & App_mask & Pump_mask & max_mask)]
    volume_fil_t_min = dt_volume[
        (Power_mask & App_mask & Turbine_mask & min_mask)]
    volume_fil_t_max = dt_volume[
        (Power_mask & App_mask & Turbine_mask & max_mask)]

    turbine_list = volume_fil_t_min['Powerstation Element'].tolist()
    pump_list = volume_fil_p_min['Powerstation Element'].tolist()

    # Dictionaries with volumes min and max
    # Pumps
    dict_min_p = {}
    dict_max_p = {}
    for entity in pump_list:
        row_min = volume_fil_p_min[volume_fil_p_min['Powerstation Element'] == entity]
        row_max = volume_fil_p_max[volume_fil_p_max['Powerstation Element'] == entity]
        list_minpT = []
        list_maxpT = []
        for i in range(1, 25):
            if i < 10:
                list_minpT.append(int(row_min['H0' + str(i)]))
                list_maxpT.append(int(row_max['H0' + str(i)]))
            else:
                list_minpT.append(int(row_min['H' + str(i)]))
                list_maxpT.append(int(row_max['H' + str(i)]))
        dict_min_p[entity] = list_minpT.copy()
        dict_max_p[entity] = list_maxpT.copy()
    # Turbines
    dict_min_t = {}
    dict_max_t = {}
    for entity in turbine_list:
        row_min = volume_fil_t_min[volume_fil_t_min['Powerstation Element'] == entity]
        row_max = volume_fil_t_max[volume_fil_t_max['Powerstation Element'] == entity]
        list_minpT = []
        list_maxpT = []
        for i in range(1, 25):
            if i < 10:
                list_minpT.append(int(row_min['H0' + str(i)]))
                list_maxpT.append(int(row_max['H0' + str(i)]))
            else:
                list_minpT.append(int(row_min['H' + str(i)]))
                list_maxpT.append(int(row_max['H' + str(i)]))
        dict_min_t[entity] = list_minpT.copy()
        dict_max_t[entity] = list_maxpT.copy()

    # Dictionaries with prices min and max
    # Pump
    dict_prices_p_min = {}
    dict_prices_p_max = {}
    for entity in pump_list:
        row = dt_price[dt_price['Powerstation Element'] == entity]
        dict_prices_p_min[entity] = int(row['Price From'])
        dict_prices_p_max[entity] = int(row['Price To'])

    # Turbines
    dict_prices_t_min = {}
    dict_prices_t_max = {}
    for entity in turbine_list:
        row = dt_price[dt_price['Powerstation Element'] == entity]
        dict_prices_t_min[entity] = int(row['Price From'])
        dict_prices_t_max[entity] = int(row['Price To'])


    # Lists for the final dataframe

    # elements
    p_element_list = []
    for a in pump_list:
        for i in range(1, 25):
            p_element_list.append(a)

    t_element_list = []
    for a in turbine_list:
        for i in range(1, 25):
            t_element_list.append(a)

    # hours
    phours = []
    for i in range(1, len(pump_list) + 1):
        for z in range(1, 25):
            phours.append(z)

    thours = []
    for i in range(1, len(turbine_list) + 1):
        for z in range(1, 25):
            thours.append(z)

    # volume
    Pvol_min = []
    Pvol_max = []
    for element in pump_list:
        Pvol_min = Pvol_min + dict_min_p[element]
        Pvol_max += dict_max_p[element]

    Tvol_min = []
    Tvol_max = []
    for element in turbine_list:
        Tvol_min += dict_min_t[element]
        Tvol_max += dict_max_t[element]

    # prices
    Pprice_min = []
    Pprice_max = []
    for element in pump_list:
        for i in range(1, 25):
            Pprice_min.append(dict_prices_p_min[element])
            Pprice_max.append(dict_prices_p_max[element])

    Tprice_min = []
    Tprice_max = []
    for element in turbine_list:
        for i in range(1, 25):
            Tprice_min.append(dict_prices_t_min[element])
            Tprice_max.append(dict_prices_t_max[element])

    # Dictionary

    Pdict = {
        "Powerstation Element": p_element_list,
        "Hour": phours,
        "Vol_min": Pvol_min,
        "Vol_max": Pvol_max,
        "Price From": Pprice_min,
        "Price To": Pprice_max
    }
    Tdict = {
        "Powerstation Element": t_element_list,
        "Hour": thours,
        "Vol_min": Tvol_min,
        "Vol_max": Tvol_max,
        "Price From": Tprice_min,
        "Price To": Tprice_max
    }

    # Dataframe
    pump_dataframe = pd.DataFrame(Pdict)
    turbine_dataframe = pd.DataFrame(Tdict)
    resul = {
        "pump":  pump_dataframe,
        "turbine": turbine_dataframe
    }

    return resul


## Filter dataframe by day and hour
def filter_h(dt, h, a, m, d):
    anyo = str(a)
    hora = str(h) if h>=10 else ("0"+str(h))
    exp = "^" + anyo + "-" + m + "-" + d + " " + hora + ":" + ".*"
    masc = dt['DeliveryStartDate'].str.contains(exp)
    return dt[masc]

## Filter dataframe by price
def filter_p_ask(dt, pr_low, pr_high):
    dt2 = dt[dt['price'] >= pr_low]
    dt2 = dt2[dt2['price'] <= pr_high]
    return dt2

def filter_p_bid (dt, pr_low, pr_high):
    dt2 = dt[dt['price'] >= pr_low]
    dt2 = dt2[dt2['price'] <= pr_high]
    return dt2

# Given a row, returns the prices range
def prices_range(price_from,price_to):
    dict_prices = {
        "Price From": float(price_from),
        "Price To": float(price_to)
    }
    return dict_prices

# Given a row, returns the volume range
def vol_range(row):
    dict_volumes = {
        "Vol_min": float(row["Vol_min"]),
        "Vol_max": float(row["Vol_max"])
    }
    return dict_volumes


# Calculates intervals given a range of prices and a max volume
def cal_interval(dict_prices, dict_vol):
    price_min = dict_prices['Price From']
    price_max = dict_prices['Price To']
    vol_min = dict_vol['Vol_min']
    vol_max = dict_vol['Vol_max']
    step = (price_max - price_min) / 10
    a = price_min

    dict_intervals = []
    for i in range(1, 11):
        interval = {
            "price_min": round(float(a),2),
            "price_max": round(float(a + step),2),
            "value": float(vol_max / 10)
        }
        dict_intervals.append(interval.copy())
        a = round(float(a + step),2)
        interval.clear()

    return dict_intervals

def searchVolumeBlock(dt_filtered, dict_prices, dict_volumen, sense):
    vol_min = dict_volumen['Vol_min']
    vol_max = dict_volumen['Vol_max']
    price_min = dict_prices['Price From']
    price_max = dict_prices['Price To']

    dt_price_filtered = dt_filtered[dt_filtered['price'] >= price_min]
    dt_price_filtered = dt_price_filtered[dt_filtered['price'] <= price_max]
    sum_volume = dt_price_filtered['vol_inc'].sum()

    if vol_min == 0:
        if vol_max < sum_volume:
            dict_volumen['Vol_min'] = 0
            dict_volumen['Vol_max'] = vol_max
        else:
            dict_volumen['Vol_min'] = 0
            dict_volumen['Vol_max'] = sum_volume
    else:
        if vol_min < sum_volume:
            if vol_max < sum_volume:
                dict_volumen['Vol_min'] = vol_min
                dict_volumen['Vol_max'] = vol_max
            else:
                dict_volumen['Vol_min'] = vol_min
                dict_volumen['Vol_max'] = sum_volume - vol_min
        else:
            dict_volumen['Vol_min'] = 0
            dict_volumen['Vol_max'] = sum_volume

    return dict_volumen

# Function for subtracting volumes
# Get a filtered dataframe a prices range and a volumes range
def sub_volumes(dt_filtered, dict_prices, dict_volumen,sense):
    vol_min = dict_volumen['Vol_min']
    vol_max = dict_volumen['Vol_max']

    price_min = dict_prices['Price From']
    price_max = dict_prices['Price To']

    if (sense == 'ask'):
        iterator = dt_filtered
    elif (sense == 'bid'):
        invertido = dt_filtered.reindex(index=dt_filtered.index[::-1])
        iterator = invertido


    if (vol_min == 0 and vol_max == 0):
        if (sense == "bid"):
            iterator = iterator.reindex(index=invertido.index[::-1])
        return iterator

    elif (vol_min == 0 and vol_max != 0) and (price_min != price_max):
        dict_intervals = cal_interval(dict_prices,dict_volumen)
        flag = False
        for z in dict_intervals:
            if flag:
                z['price_min'] = old_price_min
                z['value'] = z['value'] + old_price
                flag = False
            for row in iterator.index:
                val = iterator.at[row,'price']
                z_value = z['value']
                if iterator.at[row,'price'] >= z['price_min'] and iterator.at[row,'price'] <= z['price_max']:
                    if z['value'] != 0:
                        if iterator.at[row,'vol_inc'] == 0:
                            pass
                        else:
                            if iterator.at[row,'vol_inc'] <= z['value']:
                                z['value'] = z['value'] - iterator.at[row,'vol_inc']
                                iterator.at[row,'vol_inc'] = 0
                            else:
                                iterator.at[row,'vol_inc'] = iterator.at[row,'vol_inc'] - z['value']
                                z['value'] = 0
                                break
                    else:
                        break
                elif iterator.at[row,'price'] >= z['price_min'] and iterator.at[row,'price'] > z['price_max']:
                    old_price_min = z['price_min']
                    old_price = z['value']
                    flag = True
                    break

    elif (vol_min == 0 and vol_max != 0) and (price_min == price_max):
        ini_del = vol_max
        for row in iterator.index:
            if iterator.at[row, 'vol_inc'] < ini_del:
                ini_del = ini_del - iterator.at[row, 'vol_inc']
                iterator.at[row, 'vol_inc'] = 0
            else:
                iterator.at[row, 'vol_inc'] = iterator.at[row, 'vol_inc'] - ini_del
                break

    elif (vol_min != 0 and vol_max != 0):
        ini_del = vol_min
        flag2 = 1
        for row in iterator.index:
            if flag2 == 1:
                if iterator.at[row,'vol_inc'] < ini_del:
                    ini_del = ini_del - iterator.at[row,'vol_inc']
                    iterator.at[row,'vol_inc'] = 0
                else:
                    iterator.at[row,'vol_inc'] = iterator.at[row,'vol_inc'] - ini_del
                    ini_del = 0
                    flag2 = 0
            else:
                dict_precios2 = {
                    'Price From': iterator.at[row,'price'] if iterator.at[row,'price'] < price_max else price_max ,
                    'Price To': price_max if iterator.at[row,'price'] > price_max else price_max
                }
                dict_volumen2 = {
                    'Vol_min': 0,
                    'Vol_max': vol_max - vol_min
                }
                iterator = sub_volumes(iterator, dict_precios2, dict_volumen2, sense=sense)
                break

    if (sense == "bid"):
        iterator = iterator.reindex(index=invertido.index[::-1])

    return iterator

# Creates a new column with the acumulated volume
def acumulator(frame, type):
    if type == "ask":
        frame = frame.sort_values(['DeliveryStartDate','volume'],ascending=True)
    elif type == "bid":
        frame = frame.sort_values(['DeliveryStartDate','volume'], ascending=True)
    list_volumes = frame['volume'].tolist()
    new_column = []
    previous = 0
    first = True
    for volumen in list_volumes.copy():
        if first == True:
            value = volumen
            first = False
        else:
            value = abs(volumen - previous)
        new_column.append(value)
        previous = volumen
    frame['vol_inc'] = new_column.copy()
    return frame
