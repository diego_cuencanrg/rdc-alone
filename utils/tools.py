
import csv
import glob
import os
import datetime



def expand_dates_list(i_date, e_date, as_str=False):
    if not all([isinstance(i_date, datetime.date), isinstance(e_date, datetime.date)]):
        return []
    date_list = []
    n_days = int((e_date - i_date).days)
    for n in range(n_days + 1):
        if as_str == False:
            date_list.append(i_date + datetime.timedelta(n))
        else:
            date_list.append(str(i_date + datetime.timedelta(n)))
    return date_list

def expand_dates_hours_list(i_date, e_date, as_str=False):
    if not all([isinstance(i_date, datetime.date), isinstance(e_date, datetime.date)]):
        return []
    date_list = []
    n_days = int((e_date - i_date).days)
    for n in range(n_days + 1):
        for hour in range(24):
            if as_str == False:
                date_list.append([i_date + datetime.timedelta(n) + datetime.timedelta(hours=hour), hour])
            else:
                date_list.append([str(i_date + datetime.timedelta(n) + datetime.timedelta(hours=hour)), hour])
    return date_list

def lookahead(iterable):
    it = iter(iterable)
    last = next(it)
    for val in it:
        yield last, True
        last = val
    yield last, False

def lookahead_enumerate(iterable):
    idx = 1
    it = iter(iterable)
    last = next(it)
    for val in it:
        yield last, True, idx
        last = val
        idx += 1
    yield last, False, idx

def csv_to_list(csv_path, _delimiter=';'):
    if os.path.isfile(csv_path) == False:
        return [], 'ERROR: CSV file doesnt exists, please check path <{}>'.format(csv_path)
    lst_export = []
    with open(csv_path, 'r') as csvfile:
        idlist_row = csv.reader(csvfile, delimiter=_delimiter)
        for row in idlist_row:
            lst_element = []
            for column in row:
                lst_element.append(column)
            lst_export.append(lst_element)
    return lst_export

def traspose_array_list(lst_in):
    horizontal = len(lst_in[0])
    vertical = len(lst_in)
    lst_exit = []
    for columna in range(horizontal):
        new_row = []
        for fila in range(vertical):
            new_row.append(lst_in[fila][columna])
        lst_exit.append(new_row)
    return lst_exit

def list_array_to_text(lst_in, delimiter=','):
    output = ''
    for row, nxt in lookahead(lst_in):
        output += list_to_text(row)
        if nxt:
            output += '\n'
    return output

def list_to_text(lst_in, delimiter=','):
    output = ''
    for value, nxt in lookahead(lst_in):
        if nxt:
            output += '{}{}'.format(str(value), delimiter)
        else:
            output += '{}{}'.format(str(value), '')
    return output

def parse_json_file(json_file):
    import json
    with open(json_file) as data_file:
        json_con_data = json.load(data_file)
    return json_con_data

def check_logger(logger):
    import logging
    if all([logger, isinstance(logger, logging.Logger)]):
        return logger
    else:
        logging.basicConfig(level=logging.DEBUG)
        _logger = logging.getLogger(__name__)
        return _logger

def check_network(*, host='8.8.8.8', port=53, timeout=3):
    import socket
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except Exception as ex:
        return False

def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0:
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)
        
def rolling_to_natural(position, valoration_date, product):
    if product[0] == 'D':
        add_first = valoration_date + datetime.timedelta(int(product[1]))
        add_last = valoration_date + datetime.timedelta(int(product[1]))
    elif product[0] == 'W':
        delta = 7 * (int(product[1]) - 1)
        add_first = next_weekday(valoration_date + datetime.timedelta(delta), 0)
        add_last = next_weekday(valoration_date + datetime.timedelta(delta), 6)
    if position == 'first':
        return add_first
    else:
        return add_last


def avg(*,lst, no_zeros=False, no_nones=False):
    if not no_zeros and not no_nones:
        return sum(lst)/len(lst)
    if no_nones:
        len_lst = 0
        sum_lst = 0
        for value in lst:
            if value != None:
                len_lst += 1
                sum_lst += value
        return sum_lst / len_lst
    if no_zeros:
        len_lst = 0
        for value in lst:
            if value > 0:
                len_lst += 1
        return sum(lst) / len_lst

def createDirectory (path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass



def existCurve1and2(name, name2):
    fecha = name.split("_")[1]
    if (fecha in name2) and ("Curva2" in name2):
        return True
    else:
        return False



def list_toString(list):
    query = ""
    cont = 0
    for element in list:
        if cont == 0:
            query = str(element)
        else:
            query = query + "," + str(element)
        cont += 1
    return query